﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CostSplitDAL.CostSplitServices
{
    public class UsersGroupsService
    {
        public CostSplitDBContext context = new CostSplitDBContext();

        public int addUsersGroups(UsersGroups newUsersGroups)
        {
            context.UsersGroups.Add(newUsersGroups);
            context.SaveChanges();
            return context.UsersGroups.Where(x => x.userGroupId == newUsersGroups.userGroupId).FirstOrDefault().userGroupId;
        }

        public List<UsersGroups> getUsersGroupsByGroupId(int groupId)
        {
            return context.UsersGroups.Where(x => x.groupId == groupId).ToList();
        }

        public List<UsersGroups> getUsersGroupsByUserId(int userId)
        {
            return context.UsersGroups.Where(x => x.userId == userId).ToList();
        }

        public UsersGroups getUserGroupByUserGroupId (int userGroupId)
        {
            return context.UsersGroups.Where(x => x.userGroupId == userGroupId).FirstOrDefault();
        }

        public void changeUserGroupStatus (int userGroupId , bool response)
        {
            var userGroup = getUserGroupByUserGroupId(userGroupId);
            userGroup.status = response;
            context.SaveChanges();
        }
    }
}
