﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CostSplitDAL
{
    public class UserService
    {
        public CostSplitDBContext context = new CostSplitDBContext();

        public List<Users> getAllUsers()
        {
          return context.Users.OrderByDescending(x => x.created_at).ToList();
        }

        public Users getUser(int userId)
        {
            return context.Users.Where(x => x.userId == userId).FirstOrDefault();
        }

        public int createUser( Users createdUser)
        {
            context.Users.Add(createdUser);
            context.SaveChanges();

            return context.Users.Where(x => x.userId == createdUser.userId).FirstOrDefault().userId;
        }

        public void updateUser (Users newUser)
        {
            Users oldUser = context.Users.Where(x => x.userId == newUser.userId).FirstOrDefault();
            oldUser.firstName = newUser.firstName;
            oldUser.lastName = newUser.lastName;
            oldUser.userName = newUser.userName;
            oldUser.userPassword = newUser.userPassword;
        }

        public void deleteUser (int userId)
        {
            Users oldUser = context.Users.Where(x => x.userId == userId).FirstOrDefault();
            context.Users.Remove(oldUser);
            context.SaveChanges();
        }

        public Users userLogin (string userName ,string userPassword)
        {
           return context.Users.Where(x => x.userName == userName && x.userPassword == userPassword).FirstOrDefault();
        }
    }
}
