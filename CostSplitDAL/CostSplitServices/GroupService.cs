﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CostSplitDAL
{
    public class GroupService
    {
        CostSplitDBContext context = new CostSplitDBContext();

        public IQueryable<Groups> getAllGroups()
        {
            return context.Groups;
        }

        public Groups getGroup(int groupId)
        {
            return context.Groups.Where(x => x.groupId == groupId).FirstOrDefault();
        }

        public int createGroup(Groups group)
        {
            var addedGroup = context.Groups.Add(group);
            context.SaveChanges();

            return addedGroup.groupId;
        }

        public void editGroup (Groups newGroup)
        {
            Groups oldGroup = context.Groups.Where(x => x.groupId == newGroup.groupId).FirstOrDefault();
            oldGroup.groupName = newGroup.groupName;
            oldGroup.updated_at = DateTime.Now;
            context.SaveChanges();
        }

        public void deleteGroup (int groupId)
        {
            Groups oldGroup = context.Groups.Where(x => x.groupId == groupId).FirstOrDefault();
            context.Groups.Remove(oldGroup);
            context.SaveChanges();
        }
    }
}
