﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CostSplitDAL.CostSplitServices
{
    public class TaskService
    {
        CostSplitDBContext context = new CostSplitDBContext();

        public IQueryable<Tasks> getAllTasks(int groupId)
        {
            return context.Tasks;
        }

        public Tasks getTask (int taskId)
        {
            return context.Tasks.Where(x => x.taskId == taskId).FirstOrDefault();
        }

        public int createTask (Tasks createdTask)
        {
            context.Tasks.Add(createdTask);
            context.SaveChanges();
            return context.Tasks.Where(x => x.taskId == createdTask.taskId).FirstOrDefault().taskId;
        }

        public void editTask (Tasks newTask)
        {
            Tasks oldTask = context.Tasks.Where(x => x.taskId == newTask.taskId).FirstOrDefault();
            oldTask.taskName = newTask.taskName;
            oldTask.investment = newTask.investment;
            oldTask.cost = newTask.cost;
            oldTask.description = newTask.description;
            oldTask.updated_at = DateTime.Now;
            context.SaveChanges();
        }

        public void deleteTask (int taskId)
        {
            Tasks oldTask = context.Tasks.Where(x => x.taskId == taskId).FirstOrDefault();
            context.Tasks.Remove(oldTask);
            context.SaveChanges(); 
        }
    }
}
