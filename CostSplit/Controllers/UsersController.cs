﻿using CostSplit.Models;
using CostSplitDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using CostSplit.Mappers;
using CostSplitDAL.CostSplitServices;

namespace CostSplit.Controllers
{
    public class UsersController : Controller
    {
        private UserService userService;
        private UsersGroupsService usersGroupsService;
        
        public UsersController()
        {
            userService = new UserService();
            usersGroupsService = new UsersGroupsService();
        }
        
        
        // GET: Users
        // Login View
        public ActionResult Index()
        {
            if (HttpContext.User.Identity.IsAuthenticated)
                return Redirect("/Groups");

            return View();
        }

        [HttpPost]
        public ActionResult Login(UserLoginVM userModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View(userModel);

                var user = userService.userLogin(userModel.userName, userModel.userPassword);

                if (user == null)
                    return View("Index", userModel);

                var cookieData = new {
                    username = user.userName,
                    userId = user.userId,
                    name = user.firstName + " " + user.lastName
                };

                var serializedCookieData = new JavaScriptSerializer().Serialize(cookieData);

                //kreiranje ticketa
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, userModel.userName, DateTime.Now, DateTime.Now.AddHours(3), true, serializedCookieData);
                //hashiranje ticketa
                string hashTicket = FormsAuthentication.Encrypt(ticket);
                //kreiranje cookiea na osnovu hashiranog ticketa
                HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, hashTicket);
                //dodavanje cookia browseru
                Response.Cookies.Add(authCookie);

                return RedirectToAction("Index", "Groups");
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpGet]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index");
        }

        // GET: Users/Details/5
        public ActionResult Details(int id)
        {
            if (id <= 0)
                return new HttpNotFoundResult();

            return View(userService.getUser(id));
        }

        // GET: Users/Create
        // Register View
        public ActionResult Create() {

            if (HttpContext.User.Identity.IsAuthenticated)
                return Redirect("/Groups");

            return View();
        } 

        // POST: Users/Create
        // Redirect GroupsController
        [HttpPost]
        public ActionResult Create(UserRegisterVM userModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View(userModel);

                int userId = userService.createUser(new Users()
                {
                    firstName = userModel.firstName,
                    lastName = userModel.lastName,
                    userName = userModel.userName,
                    userPassword = userModel.userPassword,
                    created_at = DateTime.Now
                });

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Users/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }
        
        // POST: Users/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Users/Invite?groupId=5
        public ActionResult Invite(int groupId )
        {
            ViewBag.groupId = groupId;
            var users = userService.getAllUsers();
            var usersGroups = usersGroupsService.getUsersGroupsByGroupId(groupId);
            var list3 = users.Where(x => !usersGroups.Any(y => x.userId == y.userId)).ToList().MapUsersToUsersVM();
            //istrazit kako uzeti iz jedne liste koja nema elemente iz druge liste
            //izvaditi listu svih usera 
            //izvaditi listu svih usera koji su u proslijedjenoj grupi (UsersGroups)
            return View(list3);
        }

        [HttpGet]
        public ActionResult SendRequest(int userId ,int groupId)
        {
            usersGroupsService.addUsersGroups(new UsersGroups
            {
                userId = userId,
                groupId = groupId,
                status = null
            }); 
            return RedirectToAction("Invite", groupId);
        }

        public ActionResult ListOfInvitedUsers(int groupId) {
            var usersOnGroup = usersGroupsService.getUsersGroupsByGroupId(groupId).Where(x => x.status == true).Select(x => new UserVM()
            {
                firstName = x.Users.firstName,
                lastName = x.Users.lastName,
                userId = x.Users.userId,
                userName = x.Users.userName
            }).ToList();

            return PartialView("_usersGroupsList", usersOnGroup);
        }

        public ActionResult Notification(int userId )
        {
            var usersGroups = usersGroupsService.getUsersGroupsByUserId(userId).Where(x => x.status == null).ToList().MapUsersGroupsToUsersGroupsVM();
            return View(usersGroups);
        }

        [HttpGet]
        public ActionResult StatusChange (int userGroupId , bool response)
        {
            try
            {
                usersGroupsService.changeUserGroupStatus(userGroupId, response);
                return RedirectToAction("Index","Groups");
            }
            catch (Exception e)
            {

                throw;
            }
        }
    }
}
