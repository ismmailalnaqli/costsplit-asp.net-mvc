﻿using CostSplit.Helpers;
using CostSplit.Models;
using CostSplitDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using CostSplit.Mappers;

namespace CostSplit.Controllers
{
    [Authorize]
    public class GroupsController : Controller
    {
        GroupService groupService;

        public GroupsController()
        {
            this.groupService = new GroupService();
        }

        // GET: Groups
        public ActionResult Index()
        {
            var groups = groupService.getAllGroups();

            return View(groups.ToList().MapGroupsToGroupsVM());
        }

        // GET: Groups/Details/5
        public ActionResult Details(int id)
        {
            // implement details 
            ViewBag.groupId = id;
            return View(groupService.getGroup(id).MapGroupToGroupVM());
        }

        // GET: Groups/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Groups/Create
        [HttpPost]
        public ActionResult Create(GroupCreateVM groupModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View(groupModel);

                var groupId = groupService.createGroup(new Groups {
                    groupName = groupModel.groupName,
                    createdByUserId = CookiReader.getAllCookieData().userId,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now
                });

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Groups/Edit/5
        public ActionResult Edit(int id)
        {
            return View(groupService.getGroup(id).MapGroupToGroupVM());
        }

        // POST: Groups/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, GroupVM editedGroup)
        {
            try
            {
                groupService.editGroup(editedGroup.MapGroupVMToGroup());
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Groups/Delete/5
        public ActionResult Delete(int id)
        {
            return View(groupService.getGroup(id).MapGroupToGroupVM());
        }

        // POST: Groups/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            try
            {
                groupService.deleteGroup(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
