﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CostSplit.Helpers;
using CostSplit.Models;
using CostSplitDAL;
using CostSplit.Mappers;
using CostSplitDAL.CostSplitServices;

namespace CostSplit.Controllers
{
    [Authorize]
    public class TasksController : Controller
    {
        TaskService taskService;

        public TasksController()
        {
            this.taskService = new TaskService();
        }

        // GET: Tasks
        public ActionResult Index(int groupId)
        {
            ViewBag.groupId = groupId;

            return View(taskService.getAllTasks(groupId).ToList().MapTasksToTasksVM());
        }

        public ActionResult Details(int id) {
            return View(taskService.getTask(id).MapTaskToTaskVM());
        }

        // GET: Tasks/Create
        public ActionResult Create(int groupId)
        {
            return View(new TaskCreateVM { groupId = groupId });
        }

        // POST: Tasks/Create
        [HttpPost]
        public ActionResult Create(TaskCreateVM taskModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View(taskModel);

                var taskId = taskService.createTask(new Tasks
                {
                    groupId = taskModel.groupId,
                    taskName = taskModel.taskName,
                    description = taskModel.description,
                    cost = taskModel.costs,
                    investment = taskModel.investment,
                    createdByUserId = CookiReader.getAllCookieData().userId,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now
                });

                return RedirectToAction("Index", new { groupId = taskModel.groupId });
            }
            catch (Exception e)
            {
                return View();
            }
        }


        // GET: Tasks/Edit/5
        // POST: Tasks/Edit


        // GET: Tasks/Delete/5
        public ActionResult Delete(int id)
        {
            return View(taskService.getTask(id).MapTaskToTaskVM());
        }

        // POST: Tasks/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(int id, int groupId)
        {
            try
            {
                // TODO: Add delete logic here
                taskService.deleteTask(id);
                return RedirectToAction("Index", new { groupId = groupId });
            }
            catch
            {
                return View();
            }
        }
    }
}
