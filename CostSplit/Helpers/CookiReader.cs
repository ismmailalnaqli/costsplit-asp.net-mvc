﻿using CostSplit.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace CostSplit.Helpers
{
    public static class CookiReader
    {
        public static CookieData getAllCookieData() {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
                return null;

            var userData = ((FormsIdentity)HttpContext.Current.User.Identity).Ticket.UserData;

            if (userData == null || userData == String.Empty)
                return null;

            return new JavaScriptSerializer().Deserialize<CookieData>(userData);
        }
    }
}