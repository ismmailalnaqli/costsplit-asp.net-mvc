﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostSplit.Models
{
    public class UsersGroupsVM
    {
        public int userGroupId { get; set; }
        public string groupLeader { get; set; }
        public string groupName { get; set; }
        public bool? status { get; set; }
    }
}