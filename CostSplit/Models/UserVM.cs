﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CostSplit.Models
{
    public class UserVM
    {
        public string userName { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public int userId { get; set; }
    }

    public class UserLoginVM
    {
        [Required]
        public string userName { get; set; }
        [Required]
        public string userPassword { get; set; }
    }

    public class UserRegisterVM
    {
        [Required]
        public string firstName { get; set; }
        [Required]
        public string lastName { get; set; }
        [Required]
        public string userName { get; set; }
        [Required]
        public string userPassword { get; set; }
    }
}