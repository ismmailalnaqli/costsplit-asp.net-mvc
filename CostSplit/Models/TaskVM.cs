﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CostSplit.Models
{
    public class TaskVM
    {
        public int taskId { get; set; }
        public string taskName { get; set; }
        public string description { get; set; }
        public decimal costs { get; set; }
        public decimal investment { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public string createdByUser { get; set; }
        public int createdByUserId { get; set; }
        public string groupName { get; set; }
        public int groupId { get; set; }
    }

    public class TaskCreateVM
    {
        public int groupId { get; set; }
        [Required]
        public string taskName { get; set; }
        [Required]
        public string description { get; set; }
        [Required]
        public decimal costs { get; set; }
        [Required]
        public decimal investment { get; set; }
    }
}