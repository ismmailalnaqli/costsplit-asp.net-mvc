﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CostSplit.Models
{
    public class GroupVM
    {
        public int groupId { get; set; }
        public string groupName { get; set; }
        public string createdByUser { get; set; }
        public int createdByUserId { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
    }

    public class GroupCreateVM {
        [Required]
        public string groupName { get; set; }
    }
}