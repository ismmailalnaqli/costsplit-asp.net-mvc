﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostSplit.Models
{
    public  class CookieData
    {
        public  int userId { get; set; }
        public  string name { get; set; }
        public  string username { get; set; }
    }
}