﻿using CostSplit.Models;
using CostSplitDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostSplit.Mappers
{
    public static class GroupsMapper
    {
        // group list to group vm list 
        public static List<GroupVM> MapGroupsToGroupsVM(this List<Groups> groups) {

            List<GroupVM> groupsVM = new List<GroupVM>();

            foreach (var group in groups)
            {
                groupsVM.Add(new GroupVM {
                    groupId = group.groupId,
                    groupName = group?.groupName,
                    created_at = group?.created_at != null ? group.created_at.Value.ToString("dd.MM.yyyy"): "",
                    updated_at = group?.updated_at != null ? group.updated_at.Value.ToString("dd.MM.yyyy"): "",
                    createdByUser = group?.Users.firstName + " " + group.Users.lastName,
                    createdByUserId = group.createdByUserId.Value
                });
            }

            return groupsVM;
        }

        // group vm list to group list 
        public static List<Groups> MapGroupsVMToGroups (this List<GroupVM> groupsVM)
        {
            List<Groups> groups = new List<Groups>();

            foreach ( var groupVM in groupsVM)
            {
                groups.Add(new Groups
                {
                    groupId = groupVM.groupId,
                    groupName = groupVM?.groupName,
                    created_at = Convert.ToDateTime(groupVM.created_at),
                    updated_at = Convert.ToDateTime(groupVM.updated_at),
                    createdByUserId = groupVM.createdByUserId
                });
            }

            return groups;
        }

        // group to group vm
        public static GroupVM MapGroupToGroupVM(this Groups group)
        {
            GroupVM groupVM = new GroupVM();

            return new GroupVM
            {
                groupId = group.groupId,
                groupName = group?.groupName,
                created_at = group?.created_at != null ? group.created_at.Value.ToString("dd.MM.yyyy") : "",
                updated_at = group?.updated_at != null ? group.updated_at.Value.ToString("dd.MM.yyyy") : "",
                createdByUser = group?.Users.firstName + " " + group.Users.lastName,
                createdByUserId = group.createdByUserId.Value
            };
        }

        // group vm to group
        public static Groups MapGroupVMToGroup(this GroupVM groupVM)
        {
            Groups group = new Groups();

            return new Groups
            {
                groupId = groupVM.groupId,
                groupName = groupVM?.groupName,
                created_at = Convert.ToDateTime(groupVM.created_at),
                updated_at = Convert.ToDateTime(groupVM.updated_at),
                createdByUserId = groupVM.createdByUserId
            };
        }
    }
}