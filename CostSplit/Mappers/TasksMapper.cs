﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CostSplit.Models;
using CostSplitDAL;

namespace CostSplit.Mappers
{
    public static class TasksMapper
    {
        // task list to task vm list
        public static List<TaskVM> MapTasksToTasksVM(this List<Tasks> tasks)
        {
            List<TaskVM> tasksVM = new List<TaskVM>();

            foreach (var task in tasks)
            {
                tasksVM.Add(new TaskVM
                {
                    taskId = task.taskId,
                    taskName = task?.taskName,
                    description = task?.description,
                    costs = task.cost,
                    investment = task.investment.Value,
                    created_at = task?.created_at != null ? task.created_at.Value.ToString("dd.MM.yyyy") : "",
                    updated_at = task?.updated_at != null ? task.updated_at.Value.ToString("dd.MM.yyyy") : "",
                    createdByUser = task?.Users.firstName + " " + task.Users.lastName,
                    groupId = task.groupId,
                    groupName = task.Groups.groupName
                });
            }

            return tasksVM;
        }

        // task vm list to task list
        public static List<Tasks> MapTasksVMToTasks(this List<TaskVM> tasksVM)
        {
            List<Tasks> tasks = new List<Tasks>();

            foreach (var task in tasksVM)
            {
                tasks.Add(new Tasks
                {
                    taskId = task.taskId,
                    taskName = task?.taskName,
                    description = task?.description,
                    cost = task.costs,
                    investment = task.investment,
                    created_at = Convert.ToDateTime(task.created_at),
                    updated_at = Convert.ToDateTime(task.updated_at),
                    createdByUserId = task.createdByUserId,
                    groupId = task.groupId
                });
            }

            return tasks;
        }

        // task to task vm
        public static TaskVM MapTaskToTaskVM(this Tasks task)
        {
            TaskVM taskVM = new TaskVM();

            return new TaskVM
            {
                taskId = task.taskId,
                taskName = task?.taskName,
                description = task?.description,
                costs = task.cost,
                investment = task.investment.Value,
                groupName = task.Groups.groupName,
                groupId = task.groupId,
                created_at = task?.created_at != null ? task.created_at.Value.ToString("dd.MM.yyyy") : "",
                updated_at = task?.updated_at != null ? task.updated_at.Value.ToString("dd.MM.yyyy") : "",
                createdByUser = task?.Users.firstName + " " + task.Users.lastName,
                createdByUserId = task.createdByUserId.Value
            };
        }

        // task vm to task
        public static Tasks MapTaskVMToTask(this TaskVM taskVM)
        {
            Tasks task = new Tasks();

            return new Tasks
            {
                taskId = taskVM.taskId,
                taskName = taskVM?.taskName,
                description = taskVM?.description,
                cost = taskVM.costs,
                investment = taskVM.investment,
                groupId = taskVM.groupId,
                created_at = Convert.ToDateTime(taskVM.created_at),
                updated_at = Convert.ToDateTime(taskVM.updated_at),
                createdByUserId = taskVM.createdByUserId
            };
        }
    }
}