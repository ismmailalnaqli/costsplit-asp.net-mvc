﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CostSplitDAL;
using CostSplit.Models;

namespace CostSplit.Mappers
{
    public static class UsersGroupsMapper
    {
        public static List<UsersGroupsVM> MapUsersGroupsToUsersGroupsVM (this List<UsersGroups> usersGroups)
        {
            List<UsersGroupsVM> usersGroupsVM = new List<UsersGroupsVM>();

            foreach ( var item in usersGroups)
            {
                usersGroupsVM.Add(new UsersGroupsVM
                {
                    userGroupId = item.userGroupId,
                    groupLeader = item.Groups.Users.userName,
                    groupName = item.Groups.groupName,
                    status = item.status
                });
            }
            return usersGroupsVM;
        }
    }
}