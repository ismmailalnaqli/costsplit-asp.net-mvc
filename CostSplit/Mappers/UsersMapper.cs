﻿using CostSplit.Models;
using CostSplitDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CostSplit.Mappers
{
    public static class UsersMapper
    {
        public static List<UserVM> MapUsersToUsersVM(this List<Users> users)
        {
            List<UserVM> usersVM = new List<UserVM>();

            foreach (var user in users)
            {
                usersVM.Add(new UserVM
                {
                    userId = user.userId,
                    userName = user.userName,
                    firstName = user.firstName,
                    lastName = user.lastName
                });
            }

            return usersVM;
        }
    }
}